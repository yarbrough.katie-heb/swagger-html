# Authentication and Authorization Flows

These flows will show how the following concepts relate:

 - Teams and Members in the Tenancy Service
 - Groups in Active Directory for
   - Digital Teams
   - Business Users
 - Roles and Role Mappings in OneLogin
 - User Provisioning Workflows for Digital Users
   - to SCIM compliant SaaS Apps
   - to non-SCIM compliant SaaS or HEB Apps
 - Authentication and Authorization
   - Digital Users in SaaS Apps via SAML
   - Digital Users in HEB apps via OIDC from OneLogin
   - Business Users in HEB apps via OIDC from ADFS/AzureAD

### Create Engineering Team

The tenancy service will be the source of authority for engineering teams and
their members. In many cases teams will follow the HR organization structure,
but this is not a requirement.

- Create Engineering Team: tenancy service
  - admin (load from gitlab org structure)
  - team owner

##### Admin Batch Team Load

The team hierarchy will be initially populated by leverageing the gitlab.com
team hierarchy via batch load from gitlab.

```mermaid
sequenceDiagram
    participant A as Admin
    participant BS as Batch Script
    participant GL as Gitlab API
    participant T as Tenancy Service

    A ->> BS: Kick off Batch Load
    BS ->> GL: Extract Groups
    GL ->> BS: Return Groups

    BS ->> T: Create Team
```

##### Maintain Team Structure

After teams have been imported once from gitlab, they will be maintained
directly in the tenancy service. Eventually, we'd like to make all tools,
including gitlab treat the tenancy service as their single source of truth
for team hierachies.

Admins or Team Owners can edit their teams. To be a team owner, you will need
the manager role.

The User Interface here may be the H-E-B Engineering Portal.


```mermaid
sequenceDiagram
    participant O as Admin or Team Owner
    participant UI as User Interface
    participant T as Tenancy Service

    O ->> UI: Create, Move, Edit Team
    UI ->> T: Call API

```

### Manage Engineering Team Membership

Once teams are created, they need to be populated with members. This has two
pieces. A user registry of users and their details must be created. H-E-B
identity systems are authoritative for user details, but they don't actually
know who uses the engineering tool set.

By looking at Slack, Gitlab, and Atlassian (Jira & Confluence), we should be
able to identify a good starter dataset for who needs to exist in the tenancy
service. In some cases, we may be able to associate them to a team during this
load.

In general, there are three process to all people to become members of teams:

- Add member to team : tenancy service
  - admin (batch load union of slack+gitlab+atlassion)
  - team owner (maintenance)
  - autoprovisoning via SCIM: AD -> OneLogin -> Tenancy Service (see below)

##### Initial User Population

To initially seed the user registry, an extract from several tools used by
engineers will be leveraged. These include Gitlab, Slack, and Atlassian. The
user lists from these will be normalized and deduplicated using identity
data. We will take this from Azure AD since it syncs to Active Diretory and
has nice APIs via the Microsoft Graph API and has the user detail data we need.


```mermaid
sequenceDiagram
    participant A as Admin
    participant BS as Batch Script
    participant App as Gitlab/Slack/Atlassian
    participant AZ as AzureAd
    participant T as Tenancy Service

    A ->> BS: Kickoff Script
    BS ->> App: Extract User List
    App ->> BS: Return User List
    BS ->> AZ: Obtain User Details (per User)
    AZ ->> BS: Return User Details
    BS ->> T: Create User
```

##### Find User, Add to Team

It will always be possible for a Team Owner or an Admin to add people to teams
by direct fiat. If the user already exists in the tenancy service, they can
be associated with the team directly. If not, they must be loaded. As above,
we use Azure AD for this, treating Active Directory as the source of truth for
user details.

```mermaid
sequenceDiagram
    participant O as Admin or Team Owner
    participant UI as User Interface
    participant T as Tenancy Service
    participant AZ as AzureAD

    O ->> UI: Query User
    UI ->> T: GET user
    T --> T: Determine if user known
    T -->> AZ: Query Unknown User
    T --> T: Store Unknown User
    AZ -->> T: Return User Details
    T ->> UI: Return
    O ->> UI: Add User to Team
    UI ->> T: Add User to Team
    T --> T: Store User Membership
```

##### Autoprovision User to Team

It will eventually be possible to autoprovision users to tenancy teams. This
will leverage roles and role mappings managed in One Login that the tenancy
service is aware of. These autoprovision rules are ultimately under the
control of the team owner.

An example might be to add a person to a specific team if
they report up to the person who is the owner of that team. If the owner
changes, the role mapping criteria will be modified in OneLogin. When HR
changes happen in Peoplesoft, OneLogin will learn about them from Active
Directory and OneLogin will reapply it role mapping criteria and make
user provisioning calls to the Tenancy service using the SCIM API standard,
which the tenancy service will implement.

```mermaid
sequenceDiagram
    participant P as Peoplesoft
    participant AD as Active Directory
    participant OL as OneLogin
    participant T as Tenancy Service

    P ->> AD: Add/Update User
    AD ->> OL: Sync Users
    OL --> OL: Apply Role Mappings
    OL ->> T: Call SCIM Provisioning API
    T --> T: Find Team Roles
    T --> T: Add User to Team(s)
```

### Create Groups

##### Digital Team Group

- Create group for digital team: tenancy service -> ad4all -> AD -> OneLogin
  - Scoped to "Digital" teams, same as OneLogin

```mermaid
sequenceDiagram
    participant T as Tenancy Service
    participant A4 as ad4all Service
    participant AD as Active Directory
    participant OL as OneLogin

    T -->> T: Create Team
    T ->> A4: Create Team Group
    A4 ->> AD: Create Group
    AD ->> OL: Sync
```

##### Business User Group

- Create team managed group: tenancy service
  - Represents any business user (incl ones not in OneLogin)
  - Used for business apps to do authZ

```mermaid
sequenceDiagram
    participant TM as Team Member
    participant UI as User Interface
    participant T as Tenancy Service
    participant A4 as ad4all Service
    participant AD as Active Directory

    TM -->> UI: Select Tenant
    TM -->> UI: Create Business User Group<br/>under Tenant
    UI ->> T: Create Business User Group
    T --> T: Store Business User Group<br/>under Tenant
    T ->> A4: Create Team Group
    A4 ->> AD: Create Group
```

### User Grants

- Grant Group to AD user: tenancy service -> ad4all -> AD
  - Team Groups add AD -> oneLogin
  - Team managed Groups visable via AD, ADFS, AzureAD

##### Grant Group to Business User
```mermaid
sequenceDiagram
    participant TM as Team Member
    participant UI as User Interface
    participant T as Tenancy Service
    participant AZ as AzureAD
    participant A4 as ad4all Service
    participant AD as Active Directory

    TM ->> UI: Query User
    UI ->> T: GET user
    T --> T: Determine if user known
    T -->> AZ: Query Unknown User
    AZ -->> T: Return User Details
    T --> T: Store Unknown User
    T ->> UI: Return
    TM ->> UI: Add User to Business Group
    UI ->> T: Add User to Group
    T ->> A4: Add User to Group
    A4 ->> AD: Add User to Group
    AD ->> AZ: Sync Group Memberships
```

### Role Management

##### Create Role

- Define role: oneLogin
  - Apps define roles around their functionality view
  - Some curation / consolidation / rationalization process by Platform Tools Team

```mermaid
sequenceDiagram
    participant TM as Team Member
    participant PTA as Platform Tools Admin
    participant OL as OneLogin

    TM ->> PTA: Request Role for App
    PTA --> PTA: Curate Role Request
    PTA ->> OL: Create Role
```

##### Create Role Mapping

- Define RoleMapping: oneLogin
  A RoleMapping is a rule OneLogin uses to assign a role to a user
  - base role on group
  - base role on AD attributes
    - job code (eg Digital Engineering expressed as job codes)
    - samAccountName (eg contractor role: starts with vn )
    - hebworkgroupowner (eg manager role)

```mermaid
sequenceDiagram
    participant PTA as Platform Tools Admin
    participant OL as OneLogin

    PTA -->> PTA: Determine Groups and Attribute<br/>needed for Mapping
    PTA -->> SMO: Request any new AD attributes
    SMO -->> OL: Modify AD Sync Process
    PTA ->> OL: Create Mapping for Role
```

### User Provisioning

- Provisioning actions based on Roles: OneLogin, tenancy service
  - SCIM supporting SaaS apps use OneLogin->App SCIM API
  - Non-SCIM supporting apps use OneLogin->tenancy service3 SCIM API->App REST api
  - Actions: add user, update user, remove user (termination or org move)

##### SCIM Compliant App Engineering User Provisioning

Engineering users will be provisioned via SCIM to apps that support SCIM by using OneLogin roles and mappings.

```mermaid
sequenceDiagram
    participant P as Peoplesoft
    participant AD as Active Directory
    participant OL as OneLogin
    participant S as App

    P ->> AD: Add/Update User
    AD ->> OL: Sync Users
    OL -->> OL: Apply Role Mappings
    OL ->> S: Call SCIM User Provisioning API
    S -->> S: Add/Update User Details
    OL ->> S: Call SCIM Role Provisioning API
    S -->> S: Assign App Roles
```

##### SCIM Compliant App Engineering User Termination

Engineering users will be deprovisioned via SCIM to apps that support SCIM by using OneLogin as the SCIM source.

```mermaid
sequenceDiagram
    participant I as ISO
    participant SMO as SMO
    participant AD as Active Directory
    participant OL as OneLogin
    participant S as App

    I ->> SMO: Send Termination Email
    SMO ->> AD: Deprovisioning User
    AD ->> OL: Sync Users
    OL -->> OL: Determine Apps
    OL ->> S: Call SCIM User De-Provisioning API
    S -->> S: Deactivate User
```

##### Non-SCIM Compliant SaaS App Engineering User Provisioning

```mermaid
sequenceDiagram
    participant P as Peoplesoft
    participant AD as Active Directory
    participant OL as OneLogin
    participant T as Tenancy Service
    participant S as SaaS App

    P ->> AD: Add/Update User
    AD ->> OL: Sync Users
    OL -->> OL: Apply Role Mappings
    OL ->> T: Call SCIM User Provisioning API
    T -->> T: Add/Update User Details
    OL ->> T: Call SCIM Role Provisioning API
    T -->> S: Provision Roles via API
```

##### Non-SCIM Compliant App Business User Termination

```mermaid
sequenceDiagram
    participant I as ISO
    participant R as Gitlab Runner Job
    participant MG as Microsoft Graph API
    participant AZ as AzureAD
    participant E as Platform Tools Mailbox
    participant A as App

    I ->> E: Send Termination Email
    R ->> R: Scheduled Run
    R ->> AZ: Do OAuth Flow
    AZ ->> R: Return JWT Token
    R ->> MG: Retrieve Email
    MG ->> E: Read Email Box
    E ->> MG: Retrieve Unread Emails
    MG ->> R: Return Emails as JSON
    R -->> R: Parse Emails (JSON/HTML)
    R ->> A: Deactivate User
```


### Authentication and Authorization Runtime Flows

- SaaS app authenticate Digital user: OneLogin -> App
  - eg: Slack, AWS, Atlassian, Gitlab, Datadog
  - SAML Based
  - OneLogin Roles come as

##### Single Sign on to SaaS App for Digital Users

```mermaid
sequenceDiagram
    participant D as Digital User
    participant S as Saas App
    participant OL as OneLogin

    D ->> S: Access Resource
    S ->> OL: Redirect
    OL -->> OL: Authenticate to SSO Session
    OL -->> OL: Determine Roles
    OL -->> OL: Form SAML Assertion
    OL ->> S: Send SAML Assertion
    S -->> S: Create App Session
    S -->> S: Grant Roles
    S ->> D: Allow Access
```

##### Signle Sign on to HEB App for Digital Users

- Digital App authenticate Digital users: OneLogin -> Apps
  - OIDC based
  - JWT Token includes groups + roles

```mermaid
sequenceDiagram
    participant D as Digital User
    participant H as HEB App
    participant OL as OneLogin
    participant SV as HEB Service

    D ->> H: Access Resource
    H -->> H: Check for cookie (fails)
    H ->> D: Redirect to Login
    D ->> OL: Load Login Page
    H ->> OL: Do OIDC/Oauth<br/>Auth Code Flow
    OL -->> OL: Authenticate to SSO Session
    OL ->> D: Redirect to Auth Code Callback
    D ->> H: Load Callback with Auth Code
    H  ->> OL: Request Tokens
    OL -->> OL: Determine Groups & Roles
    OL -->> OL: Form JWT Claims<br/>(Groups & Roles)
    OL ->> H: Return OAuth Tokens<br/>Access, Identity, Refresh
    H -->> H: Validate, Store Tokens
    H -->> H: Set Session Cookie<br/>with Access JWT Token
    H -->> H: Use JWT Claims for RBAC Authz
    H ->> SV: Call Services using JWT Token
    SV -->> SV: Validate Token
    SV -->> SV: Use JWT Claims for RBAC Authz
    SV ->> H: Complete Call
    H ->> D: Allow Access
```

##### Single Sign on to HEB App for Business Users

- Digital App authenticate business user: AzureAD/ADFS -> App
  - Uses OIDC
  - JWT Token includes groups
  - Azure AD preferred due to team self-setup

```mermaid
sequenceDiagram
    participant D as Business User
    participant H as HEB App
    participant A as AzureAD/ADFS
    participant SV as HEB Service

    D ->> H: Access Resource
    H -->> H: Check for cookie (fails)
    H ->> D: Redirect to Login
    D ->> A: Load Login Page
    H ->> A: Do OIDC/Oauth<br/>Auth Code Flow
    A -->> A: Authenticate to SSO Session
    A ->> D: Redirect to Auth Code Callback
    D ->> H: Load Callback with Auth Code
    H  ->> A: Request Tokens
    A -->> A: Determine Groups & Roles
    A -->> A: Form JWT Claims<br/>(Groups & Roles)
    A ->> H: Return OAuth Tokens<br/>Access, Identity, Refresh
    H -->> H: Validate, Store Tokens
    H -->> H: Set Session Cookie<br/>with Access JWT Token
    H -->> H: Use JWT Claims for RBAC Authz
    H ->> SV: Call Services using JWT Token
    SV -->> SV: Validate Token
    SV -->> SV: Use JWT Claims for RBAC Authz
    SV ->> H: Complete Call
    H ->> D: Allow Access
```
